package phonesmodel;

/**
 * Class that implements a distribution which only outputs one constant number
 * (for testing).
 * 
 * @author Jeroen van Riel
 */
public class ConstantDistribution implements Distribution {

    private final int constant;
    
    public ConstantDistribution(int constant) {
        this.constant = constant;
    }
    
    @Override
    public int sample() {
        return constant;
    }
    
}
