package phonesmodel;

import phonesmodel.strategies.Strategy;

/**
 * Represents the strategy of only releasing once on a specific date (for 
 * testing).
 * 
 * @author Jeroen van Riel
 */
public class OneTimeStrategy extends Strategy {

    private final int day;
    
    public OneTimeStrategy(int day) {
        this.day = day;
    }
    
    @Override
    public boolean isReleasing(int day, int balance) {
        return this.day == day;
    }

    @Override
    public String getStrategyName() {
        return "ontTimeStrategy(" + day + ")";
    }
    
}
