package phonesmodel.strategies;

import org.junit.Test;
import static org.junit.Assert.*;
import phonesmodel.Release;
import phonesmodel.ReleaseList;
import phonesmodel.Simulation;

/**
 * Test cases for class AfterCompetitorStrategy.
 *
 * @author Jeroen van Riel
 */
public class AfterCompetitorStrategyTest {
    
    public AfterCompetitorStrategyTest() {
    }

    /**
     * Test of isReleasing method, of class AfterCompetitorStrategy.
     */
    @Test
    public void testIsReleasing() {
        System.out.println("isReleasing");
        
        int balance = 0;
        
        ReleaseList releases = new ReleaseList();
        releases.add(new Release(1, Simulation.Brand.COMPETITOR_1));
        releases.add(new Release(4, Simulation.Brand.COMPETITOR_2));
        
        int daysAfter = 2;
        AfterCompetitorStrategy instance = new AfterCompetitorStrategy(daysAfter);
        instance.setReleaseList(releases);
        
        // we must be releasing on days 3 and 6
        for (int i = 1; i <= 10; ++i) {
            assertTrue((i != 3 && i != 6) || instance.isReleasing(i, balance));
        }
    }
    
}
