/*
 */
package phonesmodel;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import phonesmodel.Simulation.Brand;

/**
 *
 * @author Jeroen van Riel
 */
public class SimulationTest {
    
    public SimulationTest() {
    }

    /**
     * Test one customer phone breaks on the day of a release.
     * 
     * Days 1-4. We release on day 3. Phone breaks on day 3.
     * Customer switches to our brand.
     */
    @Test
    public void testPhoneBreaks() {
        System.out.println("1 customer: phone breaks");
        
        int start = 1;
        int duration = 4;
        // end = 4
        
        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer(Simulation.Brand.COMPETITOR_1, 1, 2, 10));
        
        Simulation sim = new Simulation(start, duration,
                new OneTimeStrategy(3),
                new NullStrategy(),
                new NullStrategy(),
                getInitialReleasesList(),
                customers,
                new ConstantDistribution(10));
        
        sim.run();
        
        assertEquals(Brand.OUR_BRAND, sim.getCustomers().get(0).brand);
        assertEquals(3, sim.getCustomers().get(0).purchasedOn);
        assertEquals(10, sim.getCustomers().get(0).durability);
        assertEquals(10, sim.getCustomers().get(0).daysSatisfied);
        
        assertEquals(1, sim.getSales(Brand.OUR_BRAND));
        assertEquals(0, sim.getSales(Brand.COMPETITOR_1));
        assertEquals(0, sim.getSales(Brand.COMPETITOR_2));
    }
    
    /**
     * Test one customer wants new phone after day of a release.
     * 
     * Days 1-4. We release on day 2. Customer wants a new phone on day 3.
     * Customer buys a new phone of our latest release.
     */
    @Test
    public void testWantsNewPhone() {
        System.out.println("1 customer: wants new phone");
        
        int start = 1;
        int duration = 4;
        // end = 4
                
        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer(Brand.OUR_BRAND, 1, 10, 2));
        
        Simulation sim = new Simulation(start, duration,
                new OneTimeStrategy(2),
                new NullStrategy(),
                new NullStrategy(),
                getInitialReleasesList(),
                customers,
                new ConstantDistribution(10));
        
        sim.run();
        
        assertEquals(Brand.OUR_BRAND, sim.getCustomers().get(0).brand);
        assertEquals(3, sim.getCustomers().get(0).purchasedOn);
        assertEquals(10, sim.getCustomers().get(0).durability);
        assertEquals(2, sim.getCustomers().get(0).daysSatisfied);
        
        assertEquals(1, sim.getSales(Brand.OUR_BRAND));
        assertEquals(0, sim.getSales(Brand.COMPETITOR_1));
        assertEquals(0, sim.getSales(Brand.COMPETITOR_2));
    }
    
    /**
     * Tests brands releasing on the same day.
     * 
     * When brands release on the same day the priority should be:
     * OUR_BRAND
     * COMPETITOR_1
     * COMPETITOR_2
     * 
     * test case:
     * days 1 and 2,
     * durability = 1, so user wants a new phone on day 2.
     * All brands release on day 2. But OUR_COMPANY should sell.
     */
    @Test
    public void testReleaseOnSameDay() {
        System.out.println("1 customer: releases on same day");
        
        int start = 1;
        int duration = 2;
        // end = 2
        
        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer(Brand.COMPETITOR_1, 1, 1, 10));
        
        Simulation sim = new Simulation(start, duration,
                new OneTimeStrategy(2),
                new OneTimeStrategy(2),
                new OneTimeStrategy(2),
                getInitialReleasesList(),
                customers,
                new ConstantDistribution(1));
        
        sim.run();
        
        assertEquals(Brand.OUR_BRAND, sim.getCustomers().get(0).brand);
        assertEquals(2, sim.getCustomers().get(0).purchasedOn);
        assertEquals(1, sim.getCustomers().get(0).durability);
        assertEquals(10, sim.getCustomers().get(0).daysSatisfied);
        
        assertEquals(1, sim.getSales(Brand.OUR_BRAND));
        assertEquals(0, sim.getSales(Brand.COMPETITOR_1));
        assertEquals(0, sim.getSales(Brand.COMPETITOR_2));
    }
    
    /**
     * Phone breaks during waiting for newer version.
     * 
     * test case:
     * days 1-3,
     * no company releases,
     * durability = 2,
     * daysSatisfied = 1,
     * so wants new phone on day 2, but breaks on day 3,
     * therefore the customer buys OUR_COMPANY's old phone (release 1).
     */
    @Test
    public void testBreakDuringWaiting() {
        System.out.println("1 customer: phone breaks during waiting");
        
        int start = 1;
        int duration = 3;
        // end = 3
        
        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer(Brand.COMPETITOR_1, 1, 2, 1));
        
        Simulation sim = new Simulation(start, duration,
                new NullStrategy(),
                new NullStrategy(),
                new NullStrategy(),
                getInitialReleasesList(),
                customers,
                new ConstantDistribution(100));
        
        sim.run();
        
        assertEquals(Brand.OUR_BRAND, sim.getCustomers().get(0).brand);
        assertEquals(3, sim.getCustomers().get(0).purchasedOn);
        assertEquals(100, sim.getCustomers().get(0).durability);
        assertEquals(1, sim.getCustomers().get(0).daysSatisfied);
        
        assertEquals(1, sim.getSales(Brand.OUR_BRAND));
        assertEquals(0, sim.getSales(Brand.COMPETITOR_1));
        assertEquals(0, sim.getSales(Brand.COMPETITOR_2));
    }
    
    /**
     * General test case with 2 customers.
     * 
     * test case:
     * day 1-8
     * customer1: Brand = OUR_COMPANY, purchasedOn = 2, durability = 3, daysSatisfied = 2
     * customer2: Brand = COMPETITOR_1, purchasedOn = 3, durability = 2, daysSatisfied = 3
     * OUR_BRAND: new release on day 4
     * COMPETITOR_1: new release on day 5
     * COMPETITOR_2: new release on day 7
     * general durability = 3 (ConstantDistribution(3)).
     * 
     * Customer 1 buys a new phone from OUR_BRAND on day 4.
     * Customer 1's phone breaks down on day 7 and buys COMPETITOR_2's phone.
     * 
     * Customer 2's phone breaks down on day 5 and buys COMPETITOR_1's phone.
     * Customer 2's phone breaks again on day 8 and buys COMPETITOR_2's phone.
     * 
     * Totals sales: OUR_BRAND = 1, COMPETITOR_1 = 1, COMPETITOR_2 = 2.
     */
    @Test
    public void testGeneralCase() {
        System.out.println("2 customers: general case");
        
        int start = 1;
        int duration = 8;
        // end = 8
        
        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer(Brand.OUR_BRAND, 2, 3, 2));
        customers.add(new Customer(Brand.COMPETITOR_1, 3, 2, 3));
        
        Simulation sim = new Simulation(start, duration,
                new OneTimeStrategy(4),
                new OneTimeStrategy(5),
                new OneTimeStrategy(7),
                getInitialReleasesList(),
                customers,
                new ConstantDistribution(3));
        
        sim.run();
        
        // customer 1
        assertEquals(Brand.COMPETITOR_2, sim.getCustomers().get(0).brand);
        assertEquals(7, sim.getCustomers().get(0).purchasedOn);
        assertEquals(3, sim.getCustomers().get(0).durability);
        assertEquals(2, sim.getCustomers().get(0).daysSatisfied);
        
        // customer 2
        assertEquals(Brand.COMPETITOR_2, sim.getCustomers().get(1).brand);
        assertEquals(8, sim.getCustomers().get(1).purchasedOn);
        assertEquals(3, sim.getCustomers().get(1).durability);
        assertEquals(3, sim.getCustomers().get(1).daysSatisfied);
        
        // sales
        assertEquals(1, sim.getSales(Brand.OUR_BRAND));
        assertEquals(1, sim.getSales(Brand.COMPETITOR_1));
        assertEquals(2, sim.getSales(Brand.COMPETITOR_2));
    }
    
    /**
     * Create a ReleaseList with a release on day 1 for every brand.
     * Makes sure that we have the latest release.
     * @return  a ReleaseList with a release on day 1 for every brand.
     */
    private ReleaseList getInitialReleasesList() {
        ReleaseList releaseList = new ReleaseList();
        releaseList.add(new Release(1, Brand.COMPETITOR_2 ));
        releaseList.add(new Release(1, Brand.COMPETITOR_1));
        releaseList.add(new Release(1, Brand.OUR_BRAND));
        return releaseList;
    }
    
}
