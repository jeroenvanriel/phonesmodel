package phonesmodel;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jeroen van Riel
 */
public class ReleaseListTest {
    
    public ReleaseListTest() {
        
    }

    /**
     * Test of getLatestRelease method, of class ReleaseList.
     */
    @Test
    public void testGetLatestRelease_ByBrand() {
        System.out.println("getLatestRelease(brand)");
        
        ReleaseList instance = new ReleaseList();
        instance.add(new Release(1, Simulation.Brand.OUR_BRAND));
        instance.add(new Release(2, Simulation.Brand.COMPETITOR_1));
        Release rel = instance.getLatest(Simulation.Brand.OUR_BRAND);
        assertEquals(rel.date, 1);
    }

    /**
     * Test of getLatestRelease method, of class ReleaseList.
     */
    @Test
    public void testGetLatestRelease() {
        System.out.println("getLatestRelease()");
        
        ReleaseList instance = new ReleaseList();
        instance.add(new Release(1, Simulation.Brand.OUR_BRAND));
        instance.add(new Release(2, Simulation.Brand.COMPETITOR_1));
        Release rel = instance.getLatest();
        assertEquals(rel.date, 2);
    }
    
    /**
     * Tests addRelease method on stable sorting.
     */
    @Test
    public void testAddRelease_StableSort() {
        System.out.println("addRelease() stable sort");
        
        ReleaseList instance = new ReleaseList();
        instance.add(new Release(1, Simulation.Brand.OUR_BRAND));
        instance.add(new Release(1, Simulation.Brand.COMPETITOR_1));
        Release rel = instance.getLatest();
        assertEquals(rel.brand, Simulation.Brand.COMPETITOR_1);
        
        instance.add(new Release(2, Simulation.Brand.COMPETITOR_1));
        instance.add(new Release(2, Simulation.Brand.OUR_BRAND));
        rel = instance.getLatest();
        assertEquals(rel.brand, Simulation.Brand.OUR_BRAND);
    }
    
}
