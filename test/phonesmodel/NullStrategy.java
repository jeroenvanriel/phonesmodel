/*
 */
package phonesmodel;

import phonesmodel.strategies.Strategy;

/**
 * Strategy that represents never releasing anything (for testing).
 * 
 * @author Jeroen van Riel
 */
public class NullStrategy extends Strategy {

    @Override
    public boolean isReleasing(int day, int balance) {
        return false;
    }

    @Override
    public String getStrategyName() {
        return "Never release";
    }
    
}
