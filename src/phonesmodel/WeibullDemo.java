package phonesmodel;

import java.util.Arrays;
import org.apache.commons.math3.distribution.WeibullDistribution;

/**
 *
 * @author Jeroen van Riel
 */
public class WeibullDemo extends WeibullDistribution {
    
    public WeibullDemo(double shapeParameter, double scaleParameter) {
        super(shapeParameter, scaleParameter);
    }
    
    public void printSortedList(int length) {
        double[] lst = sample(length);
        
        Arrays.sort(lst);
        for (double el : lst) {
            System.out.println(el);
        }
    }

}
