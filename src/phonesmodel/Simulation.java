package phonesmodel;

import phonesmodel.strategies.Strategy;
import java.util.List;
import java.util.stream.Collectors;
import javax.naming.OperationNotSupportedException;

/**
 * This class simulates the release and sales of smart phones for a particular 
 * company that has two competitors.
 * 
 * The strategies of all companies can be set in the constructor. Further
 * options include the initial customers, initial releases, duration of
 * customer satisfaction and distribution for phone lifetimes. Invoke run() to
 * start the simulation and use the getters to obtain the final state of the 
 * simulation.
 * 
 * When brands release on the same day the priority should be:
 * OUR_BRAND, COMPETITOR_1, COMPETITOR_2.
 * 
 * @author Jeroen van Riel
 */
public class Simulation {
    
    public enum Brand { OUR_BRAND, COMPETITOR_1, COMPETITOR_2 };
    
    private final int start;
    private final int duration;
    
    private final List<Customer> customers;
    
    private final ReleaseList releaseList;
    
    private final Distribution durabilityDistribution;
    
    private final Strategy ourCompany;
    private final Strategy competitor1;
    private final Strategy competitor2;
    
    private int ourSales = 0;
    private int competitor1Sales = 0;
    private int competitor2Sales = 0;
    
    private int ourBalance = 0;
    private int competitor1Balance = 0;
    private int competitor2Balance = 0;
    
    private int releaseCost = 0;
    private int phonePrice = 0;
    
    /**
     * Initializes a new simulation.
     * 
     * The {@code start} parameter is the first day of the simulation and 
     * {@code start + duration - 1} is the last day of the simulation. Before
     * {@code start}, there can be a history in {@code releases} and 
     * {@code customers} from day 1 until (not including) {@code start}.
     * 
     * @param start  the starting day of this simulation, {@code start >= 1}
     * @param duration  the number of days to simulate (including the first day
     *      which is given by parameter {@code start})
     * @param ourCompany  our own release strategy
     * @param competitor1  the release strategy of our first competitor
     * @param competitor2  the release strategy of our first competitor
     * @param releaseList  a {@code ReleaseList} containing all releases so far
     * @param customers  a list of phone owners
     * @param durabilityDistribution  distribution to product integers that 
     *      correspond to the amount of days a phone will work without problems
     */
    public Simulation(int start,
            int duration,
            Strategy ourCompany,
            Strategy competitor1,
            Strategy competitor2,
            ReleaseList releaseList,
            List<Customer> customers,
            Distribution durabilityDistribution,
            int StartCapital0,
            int StartCapital1,
            int StartCapital2) throws IllegalArgumentException {
        if (start < 1) {
            throw new IllegalArgumentException("start must be >= 1");
        }
        
        this.start = start;
        this.duration = duration;
        this.ourCompany = ourCompany;
        this.competitor1 = competitor1;
        this.competitor2 = competitor2;
        this.releaseList = releaseList;
        this.customers = customers;
        this.durabilityDistribution = durabilityDistribution;
        this.ourBalance = StartCapital0;
        this.competitor1Balance = StartCapital1;
        this.competitor2Balance = StartCapital2;
    }
    
    /**
     * Runs this simulation with the given settings and stores the results in
     * this object.
     */
    public void run() {       
        for (int day = start; day <= getLastDay(); ++day) {
            // process releases on this day
            addReleases(day);
            
            // process sales on this day
            processSales(day);
        }
    }
    
    /**
     * Adds the releases for each brand on {@code day}.
     * 
     * The company in question must also have a sufficient bank balance to fund
     * this release.
     * When brands release on the same day the priority should be:
     * OUR_BRAND, COMPETITOR_1, COMPETITOR_2.
     * So we need to add the releases in the reversed order.
     */
    private void addReleases(int day) {
        if (competitor2.isReleasing(day, competitor2Balance)
                && competitor2Balance >= releaseCost) {
            releaseList.add(new Release(day, Brand.COMPETITOR_2));
            competitor2Balance -= releaseCost;
        }
        if (competitor1.isReleasing(day, competitor1Balance)
                && competitor1Balance >= releaseCost) {
            releaseList.add(new Release(day, Brand.COMPETITOR_1));
            competitor1Balance -= releaseCost;
        }
        if (ourCompany.isReleasing(day, ourBalance) 
                && ourBalance >= releaseCost) {
            releaseList.add(new Release(day, Brand.OUR_BRAND));
            ourBalance -= releaseCost;
        }
    }
    
    /**
     * Helper method for run().
     */
    private void processSales(int day) {
        for (Customer customer : customers) {                
            // broken phone
            if (customer.hasBrokenPhone(day)) {                    
                // assign new latest phone (regardless of brand)
                // to this customer.
                Brand brand = releaseList.getLatest().brand;

                // give this customer a new phone
                customer.brand = brand;
                customer.purchasedOn = day;
                customer.durability = durabilityDistribution.sample();

                // add a sale for the brand that sold this new phone
                addOneSale(brand);
            }

            // wants new phone
            if (customer.wantsNewPhone(day)) {
                int latest = releaseList.getLatest(customer.brand).date;

                // check if new phone of the customers current brand is
                // released
                if (customer.purchasedOn < latest) {
                    // give this customer a new phone
                    customer.purchasedOn = day;
                    customer.durability = durabilityDistribution.sample();
                    
                    // add a sale for this customer's brand
                    addOneSale(customer.brand);
                }
            }
        }
    }
    
    /**
     * Adds one to the sale counter of {@code brand} and increases the 
     * corresponding bank balance.
     */
    private void addOneSale(Brand brand) {
        if (brand == Brand.OUR_BRAND) {
            ++ourSales;
            ourBalance += phonePrice;
        } else if (brand == Brand.COMPETITOR_1) {
            ++competitor1Sales;
            competitor1Balance += phonePrice;
        } else if (brand == Brand.COMPETITOR_2) {
            ++competitor2Sales;
            competitor2Balance += phonePrice;
        }
    }
    
    /**
     * Sets the amount of money it requires for each brand to release a new 
     * phone model.
     * @param releaseCost  the cost for releasing a new phone model
     * @throws IllegalArgumentException  if {@code releaseCost < 0}
     */
    public void setReleaseCost(int releaseCost) throws IllegalArgumentException {
        if (releaseCost < 0) {
            throw new IllegalArgumentException("releaseCost < 0");
        }
        this.releaseCost = releaseCost;
    }
    
    /**
     * Sets the price of a phone, which a customer needs to pays a brand when
     * purchasing a phone from this brand.
     * @param phonePrice  the new price of a phone
     * @throws IllegalArgumentException  if {@code phonePrice < 0}
     */
    public void setPhonePrice(int phonePrice) throws IllegalArgumentException {
        if (phonePrice < 0) {
            throw new IllegalArgumentException("phonePrice < 0");
        }
        this.phonePrice = phonePrice;
    }
    
    /**
     * Sets the balance of {@code brand}.
     * @param brand  the Brand whose bank balance to set
     * @param balance  the new amount of money on the balance
     * @throws IllegalArgumentException  if {@code balance < 0}
     */
    public void setBalance(Brand brand, int balance) throws IllegalArgumentException {
        if (balance < 0) {
            throw new IllegalArgumentException("balance < 0");
        }
        if (brand == Brand.OUR_BRAND) {
            ourBalance = balance;
        } else if (brand == Brand.COMPETITOR_1) {
            competitor1Balance = balance;
        } else if (brand == Brand.COMPETITOR_2) {
            competitor2Balance = balance;
        }
    }
    
    /**
     * Gets the balance of {@code brand}.
     * @param brand  the Brand whose balance to get
     * @return  the balance of {@code brand}
     */
    public int getBalance(Brand brand) {
        if (brand == Brand.OUR_BRAND) {
            return ourBalance;
        } else if (brand == Brand.COMPETITOR_1) {
            return competitor1Balance;
        } else {
            return competitor2Balance;
        }
    }
    
    public int getStartDay() {
        return start;
    }
    
    public int getDuration() {
        return duration;
    }
    
    public int getLastDay() {
        return start + duration - 1;
    }
    
    public ReleaseList getReleaseList() {
        return releaseList;
    }
    
    public List<Customer> getCustomers() {
        return customers;
    }
    
    public int getNrCustomers(Brand brand) {
        return customers.stream().filter(c -> c.brand == brand)
                .collect(Collectors.toList()).size();
    }
    
    public int getSales(Brand brand) {
        if (brand == Brand.OUR_BRAND) {
            return ourSales;
        } else if (brand == Brand.COMPETITOR_1) {
            return competitor1Sales;
        } else if (brand == Brand.COMPETITOR_2) {
            return competitor2Sales;
        }
        throw new IllegalArgumentException("brand not yet supported");
    }
    
    public String getOurStrategyName() {
        return ourCompany.getStrategyName();
    }
    
    public String getCompetitor1StrategyName() {
        return competitor1.getStrategyName();
    }
    
    public String getCompetitor2StrategyName() {
        return competitor2.getStrategyName();
    }
    
}
