package phonesmodel.strategies;

import java.util.ArrayList;
import java.util.List;

/**
 * Basic random release strategy based on Bernoulli trials.
 * 
 * The {@code releaseRate} is the probability for a Bernoulli trial.
 * Uses the standard java Math.random() function for random number generation.
 * 
 * @author Jeroen van Riel
 */
public class RandomStrategy extends Strategy {
    
    private final int days;
    
    private final double releaseRate;
    
    private final List<Integer> releases;
    
    /**
     * Constructs a new strategy based on the maximum number of {@code days}
     * and the given {@code releaseRate}.
     * 
     * @param days  maximal number of days this strategy can be used for
     *      with the {@code days} included as the last day
     * @param releaseRate  between 0.0 and 1.0; the probability that a release 
     *      will be done at a particular day
     * @throws IllegalArgumentException  if {@code releaseRate < 0.0} or
     *      {@code releaseDate > 1.0}
     */
    public RandomStrategy(int days, double releaseRate)
            throws IllegalArgumentException {
        this.days = days;
        this.releaseRate = releaseRate;
        this.releases = new ArrayList<>();
                
        for (int i = 0; i <= days; ++i) {
            // if a random number stays below the threshold
            if (Math.random() < releaseRate) {
                // add this day to the list of release days
                releases.add(i);
            }
        }
    }
    
    /**
     * @throws IllegalArgumentException  if {@code day > this.getMaxDays()}
     */
    @Override
    public boolean isReleasing(int day, int balance)
            throws IllegalArgumentException {
        if (day > days)
            throw new IllegalArgumentException("day is larger than this.getMaxDays()");
        
        return releases.contains(day);
    }
    
    public int getMaxDays() {
        return days;
    }

    @Override
    public String getStrategyName() {
        return "RandomStrategy(" + days + ", " + releaseRate + ")";
    }
    
}
