package phonesmodel.strategies;

import phonesmodel.Simulation;

/**
 * Basic release strategy that always tries to have the latest release.
 * 
 * @author Jeroen van Riel
 */
public class LatestStrategy extends Strategy {
    
    @Override
    public boolean isReleasing(int day, int balance) {
        return releaseList.getLatest().brand != Simulation.Brand.OUR_BRAND;
    }

    @Override
    public String getStrategyName() {
        return "LatestStrategy()";
    }
    
}
