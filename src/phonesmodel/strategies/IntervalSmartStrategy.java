package phonesmodel.strategies;

import phonesmodel.Simulation;

/**
 * Basic release strategy that releases at a fixed time interval except when
 * we already have the latest release.
 * 
 * @author Jeroen van Riel
 */
public class IntervalSmartStrategy extends Strategy {
    
    private int interval;
    
    public IntervalSmartStrategy(int interval) {
        this.interval = interval;
    }
    
    @Override
    public boolean isReleasing(int day, int balance) {
        if (releaseList.getLatest().brand != Simulation.Brand.OUR_BRAND) {
            return day % interval == 0;
        }
        return false;
    }

    @Override
    public String getStrategyName() {
        return "IntervalSmartStrategy(" + interval + ")";
    }
    
}
