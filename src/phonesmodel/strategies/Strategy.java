package phonesmodel.strategies;

import phonesmodel.ReleaseList;

/**
 * Abstract class that represents a company's release strategy.
 * 
 * @author Jeroen van Riel
 */
public abstract class Strategy {
    
    /** Internal reference to the list of releases. */
    protected ReleaseList releaseList;
    
    /**
     * Returns whether the company is releasing a new phone on this day
     * 
     * @param day  the current day
     * @param balance  this company's current bank balance
     * @return  whether the company is releasing a new phone on this day
     */
    public abstract boolean isReleasing(int day, int balance);
    
    /**
     * Get a human-readable name for this strategy and it's settings.
     * @return  a String that contains a human-readable name for this strategy 
     *      and it's settings
     */
    public abstract String getStrategyName();
    
    public void setReleaseList(ReleaseList list) {
        this.releaseList = list;
    }
    
}
