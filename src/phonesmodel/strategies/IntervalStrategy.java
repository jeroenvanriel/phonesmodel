package phonesmodel.strategies;

/**
 * Basic release strategy that releases at a fixed time interval.
 * 
 * @author Jeroen van Riel
 */
public class IntervalStrategy extends Strategy {
    
    private int interval;
    
    public IntervalStrategy(int interval) {
        this.interval = interval;
    }
    
    @Override
    public boolean isReleasing(int day, int balance) {
        return day % interval == 0;
    }

    @Override
    public String getStrategyName() {
        return "IntervalStrategy(" + interval + ")";
    }
    
}
