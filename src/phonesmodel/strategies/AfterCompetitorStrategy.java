package phonesmodel.strategies;

import phonesmodel.Simulation;

/**
 * This strategy always releases a fixed time after every release of one of it's
 * competitors.
 * 
 * @author Jeroen van Riel
 */
public class AfterCompetitorStrategy extends Strategy {

    private final int daysAfter;
    
    public AfterCompetitorStrategy(int daysAfter) {
        this.daysAfter = daysAfter;
    }
    
    @Override
    public boolean isReleasing(int day, int balance) {
        boolean afterCompetitor1 = 
                releaseList.getReleaseDays(Simulation.Brand.COMPETITOR_1)
                        .contains(day - daysAfter);
        
        boolean afterCompetitor2 = 
                releaseList.getReleaseDays(Simulation.Brand.COMPETITOR_2)
                        .contains(day - daysAfter);
        
        // publish planned releases
        return afterCompetitor1 || afterCompetitor2;
    }

    @Override
    public String getStrategyName() {
        return "AfterCompetitorStrategy(" + daysAfter + ")";
    }
   
}
