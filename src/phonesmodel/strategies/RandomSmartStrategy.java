package phonesmodel.strategies;

import java.util.ArrayList;
import java.util.List;

/**
 * Random release strategy based on Bernoulli trials with a restriction that 
 * makes sure that two consecutive releases are not too close in time.
 * 
 * The {@code releaseRate} is the probability for a Bernoulli trial.
 * Uses the standard java Math.random() function for random number generation.
 * 
 * @author Jeroen van Riel
 */
public class RandomSmartStrategy extends Strategy {
    
    private final int days;
    
    private final int minimalDaysLater;
    
    private final double releaseRate;
    
    private final List<Integer> releases;
    
    /**
     * Constructs a new strategy based on the maximum number of {@code days} and
     * {@code minimalDaysInBetween} and the given {@code releaseRate}.
     * 
     * @param days  maximal number of days this strategy can be used for
     *      with the {@code days} included as the last day
     * @param minimalDaysLater  minimal days that a release must be after the
     *      previous releases
     * @param releaseRate  between 0.0 and 1.0; the probability that a release 
     *      will be done at a particular day
     * @throws IllegalArgumentException  if {@code releaseRate < 0.0} or
     *      {@code releaseDate > 1.0}
     */
    public RandomSmartStrategy(int days, int minimalDaysLater,
            double releaseRate)
            throws IllegalArgumentException {
        this.days = days;
        this.minimalDaysLater = minimalDaysLater;
        this.releaseRate = releaseRate;
        this.releases = new ArrayList<>();
                
        for (int i = 0; i <= days; ++i) {
            // if a random number stays below the threshold
            // and the last release day was 
            if (Math.random() < releaseRate) {
               if (releases.size() <= 0) {
                    // the very first release
                    releases.add(i);
               } else if (i - releases.get(releases.size() - 1)
                       >= minimalDaysLater) {
                    // at least minmalDaysLater later since previous release
                    releases.add(i);
               }
            }
        }
    }
    
    /**
     * @throws IllegalArgumentException  if {@code day > this.getMaxDays()}
     */
    @Override
    public boolean isReleasing(int day, int balance)
            throws IllegalArgumentException {
        if (day > days)
            throw new IllegalArgumentException("day is larger than this.getMaxDays()");
        
        return releases.contains(day);
    }
    
    public int getMaxDays() {
        return days;
    }

    @Override
    public String getStrategyName() {
        return "RandomStrategy(" + days + ", " + minimalDaysLater + ", "
                + releaseRate + ")";
    }
    
}
