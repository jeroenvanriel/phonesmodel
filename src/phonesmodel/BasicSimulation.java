package phonesmodel;

import phonesmodel.strategies.IntervalStrategy;
import phonesmodel.strategies.RandomStrategy;
import phonesmodel.strategies.Strategy;
import java.util.ArrayList;
import java.util.List;

/**
 * One smart phone release simulation.
 * 
 * This class configures and runs a particular simulation and gathers the
 * results.
 * 
 * @author Jeroen van Riel
 */
public class BasicSimulation {

    /**
     * Used for the amount of time a customer is satisfied with his/her phone.
     */
    public static final int DAYS_IN_YEAR = 365;
    // days after which a customer is looking for an upgrade
    public int DAYS_SATISFIED = DAYS_IN_YEAR;
    // days we simulate
    public int DAYS_SIMULATE = 4 * DAYS_IN_YEAR;
    // weibull distribution variables
    public double WEIBULL_K = 4;//1.5;
    public double WEIBULL_L = 606.794;//394.353;
    // ours and competitors starting customers
    public int START_CONS_0 = 20;
    public int START_CONS_1 = 20;
    public int START_CONS_2 = 20;
    
    public int START_DAY = 365;
    
    public int START_CAPITAL_0 = 0;
    public int START_CAPITAL_1 = 0;
    public int START_CAPITAL_2 = 0;
    
    public int RELEASE_COST = 0;
    public int PHONE_COST = 0;
        
    public Strategy ourStrategy;
    public Strategy competitor1Strategy;
    public Strategy competitor2Strategy;
    
    
    
    /**
     * Perform a basic exemplary 3-year simulation starting on day 1.
     * 
     * @return  the Simulation object that we created which contains the results
     */
    public Simulation getBasicSimulation() {
        
        
        // durability of a phone
        // shapeParameter > 1 means that there is an 'aging' process.
        // See Weibull_one_year.nb for a plot of this specific distribution
        // which has a mean of 365. So on average, a phone breaks down after
        // one year.
        Distribution durability = new DiscreteWeibullDistribution(WEIBULL_K, WEIBULL_L);
        
        // generate initial phone releases
        ReleaseList releases = populateReleases();
        
        // generate initial phone owners based on the initial releases
        List<Customer> customers = new ArrayList<>();
        
        // 20 customers of our brand
        for (int i = 0; i < START_CONS_0; ++i) {
            customers.add(new Customer(Simulation.Brand.OUR_BRAND,
                    randomWithRange(1, START_DAY - 1),
                    durability.sample(),
                    DAYS_SATISFIED));
        }
        
        // 20 customers of each competitors brands
        for (int i = 0; i < START_CONS_1; ++i) {
            customers.add(new Customer(Simulation.Brand.COMPETITOR_1,
                    randomWithRange(1, START_DAY - 1),
                    durability.sample(),
                    DAYS_SATISFIED)); 
        }
        for (int i = 0; i < START_CONS_2; ++i) {
            customers.add(new Customer(Simulation.Brand.COMPETITOR_2,
                    randomWithRange(1, START_DAY - 1),
                    durability.sample(),
                    DAYS_SATISFIED));
        }
        
        
        ourStrategy.setReleaseList(releases);
        competitor1Strategy.setReleaseList(releases);
        competitor2Strategy.setReleaseList(releases);

        // create new simulation with the following strategies
        // Our strategy: approx. 11.87 releases per year
        // Competitor 1: approx. 7.12 releases per year
        // Competitor 2: approx. 7.12 releases per year
        Simulation sim = new Simulation(START_DAY, DAYS_SIMULATE,
                ourStrategy,
                competitor1Strategy,
                competitor2Strategy,
                releases,
                customers,
                durability,
                START_CAPITAL_0,
                START_CAPITAL_1,
                START_CAPITAL_2);
        
        // run the simulation on these settings
        sim.setPhonePrice(PHONE_COST);
        sim.setReleaseCost(RELEASE_COST);
        sim.run();
        
        return sim;
    }
    
    private ReleaseList populateReleases() {
        ReleaseList releases = new ReleaseList();
        releases.add(new Release(1, Simulation.Brand.OUR_BRAND));
        releases.add(new Release(1, Simulation.Brand.COMPETITOR_1));
        releases.add(new Release(1, Simulation.Brand.COMPETITOR_2));
        return releases;
    }

    private int randomWithRange(int min, int max) {
       int range = (max - min) + 1;     
       return (int)(Math.random() * range) + min;
    }
    
}
