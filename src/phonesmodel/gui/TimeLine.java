package phonesmodel.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;
import javax.swing.JPanel;
import phonesmodel.BasicSimulation;

/**
 * Graphical visualization of a timeline.
 * 
 * This class always draws the start of a day. 
 * 
 * @author Jeroen van Riel
 */
public class TimeLine extends JPanel {
    
    private List<Integer> days;
    private int start;
    private int end;
    
    private int scale;
    
    private final int padding_x = 10;
    private final int line_y = 15;
    private final int tick_height = 5;
    private final int marker_height = 15;
   
    public TimeLine() {
        scale = BasicSimulation.DAYS_IN_YEAR; 
    }
    
    /**
     * Sets the days on which markers should be drawn and the range to draw.
     * 
     * @param days  list of days for which a marker should be drawn
     * @param start  first day on the timeline
     * @param end  last day on the timeline
     */
    public void setDays(List<Integer> days, int start, int end) {
        this.days = days;
        this.start = start;
        this.end = end;
        repaint();
    }
    
    /**
     * Sets the timescale used for drawing ticks.
     * @param scale  number of days between ticks
     */
    public void setScale(int scale) {
        this.scale = scale;
        repaint();
    }
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(800, 20);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        Graphics2D g2 = (Graphics2D) g;
        
        // draw base line
        g2.setColor(Color.black);
        g2.setStroke(new BasicStroke(1));
        g2.drawLine(padding_x, line_y, timeLineWidth() + padding_x, line_y);
        
        // draw ticks
        drawTicks(g2);
        
        // draw day markers
        if (days != null) {
            drawDays(g2);
        }
    }
    
    private void drawDays(Graphics2D g2) {        
        g2.setColor(Color.black);
        g2.setStroke(new BasicStroke(1));
            
        for (int day : days) {
            int x = getDayXCoord(day);            
            
            g2.drawLine(x, line_y, x, line_y - marker_height);
        }
    }
    
    private void drawTicks(Graphics2D g2) {
        int day = start;
        
        // We go one day further to include the endmarker which marks the
        // end of the last day.
        while (day <= end + 1) {
            int x = getDayXCoord(day);
            
            g2.setColor(Color.black);
            g2.setStroke(new BasicStroke(1));
            g2.drawLine(x, line_y, x, line_y + tick_height);
            
            day += scale;
        }
    }
    
    private int getDayXCoord(int day) {
        // To calculate the x coord for a marker, we need to subtract start
        // from the day count.
        double ratio = (day - start) / (double) (end - start + 1);
        return padding_x + (int) ((double) timeLineWidth() * ratio);
    }
    
    /** Gets the width of the time line itself in pixels */
    private int timeLineWidth() {
        return getWidth() - 2 * padding_x;
    }
    
}
