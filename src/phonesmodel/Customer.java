package phonesmodel;

import phonesmodel.Simulation.Brand;

/**
 * Class that represents a customer having a particular phone.
 * 
 * The phone has a brand, a purchase date and durability and each object has a
 * unique identifier that has been given during construction.
 * The customer wants a new phone after a {@code daysSatisfied}.
 * 
 * @author Jeroen van Riel
 */
public class Customer {
    
    private static int id_counter = 0;
    
    /** Unique identifier */
    private int id;
    
    /** The manufacturer of this customer's phone */
    public Brand brand;
    
    /** The date on which this customer bought his/her phone */
    public int purchasedOn;
    
    /** The amount of days that the phone will last */
    public int durability;
    
    /** The amount of days after which this customer is looking for an upgrade */
    public int daysSatisfied;
    
    public Customer(Brand brand, int purchasedOn, int durability, int daysSatisfied) {
        this.brand = brand;
        this.purchasedOn = purchasedOn;
        this.durability = durability;
        this.daysSatisfied = daysSatisfied;
        
        // make new id
        this.id = id_counter;
        // increment the static id_counter
        ++id_counter;
    }
    
    /**
     * Returns whether this customer wants a new phone on {@code day}
     * 
     * @param day  the current day
     * @return  whether this customer wants a new phone on {@code day}
     */
    public boolean wantsNewPhone(int day) {
        return day >= purchasedOn + daysSatisfied;
    }
    
    /**
     * Returns whether this customer has a broken phone on {@code day}.
     * 
     * @param day  the current day
     * @return  whether this customer has a broken phone on {@code day}.
     */
    public boolean hasBrokenPhone(int day) {
        return day >= purchasedOn + durability;
    }
    
    /**
     * Gets a unique identifier for this customer.
     * 
     * @return  unique identifier for this customer
     */
    public int getId() {
        return id;
    }
    
}
