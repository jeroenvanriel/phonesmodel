package phonesmodel;

import org.apache.commons.math3.distribution.WeibullDistribution;

/**
 * Implementation of a discrete variant of the Weibull distribution.
 * 
 * This class makes use of the normal (continuous) Weibull distribution and 
 * rounds the values to the next bigger integer. This corresponds to the event 
 * of a phone breaking down during the day so that the customer needs a new 
 * phone from the next day on.
 * 
 * @author Jeroen van Riel
 */
public class DiscreteWeibullDistribution implements Distribution {

    private WeibullDistribution weibull;
    
    public DiscreteWeibullDistribution(double shapeParameter,
            double scaleParameter) {
        weibull = new WeibullDistribution(shapeParameter, scaleParameter);
    }
    
    @Override
    public int sample() {
        // We round to the next integer.
        // This corresponds to the event of a phone breaking down during the day
        // so that the customer needs a new phone from the next day on.
        return (int) Math.ceil(weibull.sample());
    }
    
}
