package phonesmodel;

/**
 * Class that records a particular release of a phone brand.
 * 
 * Releases can be compared to each other on the basis of the date.
 * 
 * @author Jeroen van Riel
 */
public class Release implements Comparable<Release> {
    
    public final int date;
    public final Simulation.Brand brand;

    public Release(int date, Simulation.Brand brand) {
        this.date = date;
        this.brand = brand;
    }
    
    @Override
    public int compareTo(Release release) {
        return date - release.date;
    }

}