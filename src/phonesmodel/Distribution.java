package phonesmodel;

/**
 * Interface to allow for random generation of integers.
 * 
 * An implementing class can for example provide samples from a specific random
 * distribution.
 * 
 * @author Jeroen van Riel
 */
public interface Distribution {
    
    /**
     * Gets next random integer from the distribution.
     * 
     * @return  next random integer from the distribution
     */
    public int sample();
    
}
