package phonesmodel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import phonesmodel.Simulation.Brand;

/**
 * This class keeps track of releases for each brand.
 * 
 * The sorting algorithm that this class uses is stable, which means that when
 * two brand have both a release on the same date, the methods in this class
 * will return the release that was last added to this list.
 * 
 * @author Jeroen van Riel
 */
public class ReleaseList {
    
    /*
     * Representation invariants:
     * - dates in releases are in ascending order
     */
    
    /** List of all releases */
    private List<Release> releases;
    
    public ReleaseList() {
        releases = new ArrayList<>();
    }
    
    /**
     * Returns the latest Release of {@code brand}
     * 
     * @param brand  the brand to pick the latest release from
     * @return latest release date from brand, null if brand has not released
     * anything yet
     */
    public Release getLatest(Brand brand) {
        // get the list of all releases of this brand
        List<Release> rels = releases.stream().filter(r -> r.brand == brand)
                .collect(Collectors.toList());
        // if there are any releases yet for this brand
        if (rels.size() > 0) {
            // latest release is last element in this list
            return rels.get(rels.size() - 1);
        } else {
            return null;
        }
    }
    
    /**
     * Returns the latest Release regardless of brand.
     * 
     * @return latest release date and brand from any brand, null if none exist
     */
    public Release getLatest() {
        if (releases.size() > 0) {
            // return the last added element
            return releases.get(releases.size() - 1);
        } else {
            return null;
        }
    }
    
    /**
     * Adds a new release to the list.
     * 
     * @param release  release to be added to the list
     */
    public void add(Release release) {
        releases.add(release);
        // make sure the invariant is kept
        Collections.sort(releases);
    }
    
    /**
     * Get list of all releases.
     * @return  list of all releases
     */
    public List<Release> getListOfReleases() {
        return releases;
    }
    
    /**
     * Gets a sorted list of all release dates of {@code brand}
     * @param brand  the brand for which the release dates should be fetched
     * @return  a sorted list of all release dates of {@code brand}
     */
    public List<Integer> getReleaseDays(Brand brand) {
        // get the list of all release dates of this brand
        List<Integer> rels = releases.stream().filter(r -> r.brand == brand)
                .map(r -> r.date).collect(Collectors.toList());
        // sort this list
        Collections.sort(rels);
        return rels;
    }
    
}
